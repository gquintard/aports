# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: fossdd <fossdd@pwned.life>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=py3-websockets
_pkgname=websockets
pkgver=14.0
pkgrel=0
pkgdesc="An implementation of the WebSocket Protocol (RFC 6455)"
url="https://websockets.readthedocs.io/"
arch="all"
license="BSD-3-Clause"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/aaugustin/websockets/archive/$pkgver.tar.gz
	skip-reconnect-test.patch
	"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # fail for now with context manager errors

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	export WEBSOCKETS_TESTS_TIMEOUT_FACTOR=30
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
a32d6338ed70de0b99298e9b26c6a3f18a3b517633754c8b1fdbebfb407023f73d212e98db909a276c58aab6487db501540db0c8237280c1a311d8b0e7c1e4be  py3-websockets-14.0.tar.gz
0849fa337f6a00297774cc3a3dd61497bbc99dc3b12632a385056a82e50fd04bf057068eeef6f9b705c257e3017b127ee38183461615d09549145889767bdc0b  skip-reconnect-test.patch
"
